import {Component, Input} from "@angular/core";
import {ArticleService} from "../services/";
import {Article, ArticleListConfig} from "../models/";

@Component({
    selector: 'article-list',
    templateUrl: './article-list.component.html'
})
export class ArticleListComponent{
    constructor(
        private articleService: ArticleService
    ){}

    query: ArticleListConfig;
    articles: Article[];
    loading: boolean = false;
    currentPage: number = 1;
    totalPages: Array<number> = [1];

    @Input() limit: number;
    @Input()
    set config(config: ArticleListConfig){
        if(config){
            this.query = config;
            this.currentPage = 1;
            this.runQuery();
        }
    }

    runQuery(){
        this.loading = true;
        this.articles = [];

        // create limit and offset filter (if necessary)
        if(this.limit){
            this.query.filters.limit = this.limit;
            this.query.filters.offset = (this.limit * (this.currentPage -1))
        }
        this.articleService.query(this.query)
            .subscribe(data => {
                this.loading = false;
                this.articles = data.articles;
                // Used from http://www.jstips.co/en/create-range-0...n-easily-using-one-line/
                this.totalPages = Array.from(new Array(Math.ceil(data.articlesCount / this.limit)), (val, index) => index + 1);
            });
    }

    setPageTo(pageNumber){
        this.currentPage = pageNumber;
        this.runQuery();
    }
}