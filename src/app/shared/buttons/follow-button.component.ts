import {Component, Input, EventEmitter, Output} from "@angular/core";
import {UserService, ProfileService} from "../services/";
import {Router} from "@angular/router";
import {Profile} from "../models/";

@Component({
    selector: 'follow-button',
    templateUrl: './follow-button.component.html'
})
export class FollowButtonComponent{

    constructor(
      private profileService: ProfileService,
      private router: Router,
      private userService: UserService
    ){}

    @Input() profile: Profile;
    @Output() onToggle = new EventEmitter<boolean>();
    isSubmitting = false;

    toggleFollowing(){
        this.isSubmitting = true;

        this.userService.isAuthenticated.subscribe(
            (authenticated) => {
                //Not authenticated? Push to login screen
                if(!authenticated){
                    this.router.navigateByUrl('/login');
                    return;
                }

                //Follow this profile if it isn't already
                if(!this.profile.following){
                    this.profileService.follow(this.profile.username)
                        .subscribe(
                            data => {
                                this.isSubmitting = false;
                                this.onToggle.emit(true);
                            },
                            err => this.isSubmitting = false
                        );
                //Otherwise unfollow
                } else {
                    this.profileService.unfollow(this.profile.username)
                        .subscribe(
                            data => {
                                this.isSubmitting = false;
                                this.onToggle.emit(false);
                            },
                            err => this.isSubmitting = false
                        );
                }
            }
        )
    }
}