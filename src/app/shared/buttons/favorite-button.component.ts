import {Component, Input, EventEmitter, Output} from "@angular/core";
import {ArticleService, UserService} from "../services/";
import {Router} from "@angular/router";
import {Article} from "../models/";

@Component({
    selector: 'favorite-button',
    templateUrl: './favorite-button.component.html'
})
export class FavoriteButtonComponent{
    constructor(
        private articleService: ArticleService,
        private router: Router,
        private userService: UserService
    ){}
    @Input() article: Article;
    @Output() onToggle = new EventEmitter<boolean>();
    isSubmitting = false;

    toggleFavorite(){
        this.isSubmitting = true;

        this.userService.isAuthenticated.subscribe(
            (authenticated) => {
                //Not authenticated? Push to login screen
                if(!authenticated){
                    this.router.navigateByUrl('/');
                    return;
                }

                if(!this.article.favorited){
                    this.articleService.favorite(this.article.slug)
                        .subscribe(
                            data => {
                                this.isSubmitting = false;
                                this.onToggle.emit(true);
                            },
                            err => {
                                this.isSubmitting = false;
                            }
                        )
                } else {
                    this.articleService.unfavorite(this.article.slug)
                        .subscribe(
                            data => {
                                this.isSubmitting = false;
                                this.onToggle.emit(false);
                            },
                            err => {
                                this.isSubmitting = false;
                            }
                        )
                }

            }
        )
    }

}