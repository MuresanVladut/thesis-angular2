import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {ListErrorsComponent} from "./list-errors.component";
import {ShowAuthedDirective} from "./show-authed.directive";
import {FollowButtonComponent,FavoriteButtonComponent} from "./buttons";
import {ArticleMetaComponent, ArticleListComponent, ArticlePreviewComponent} from "./article-helpers/";

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
      RouterModule
  ],
  declarations: [
      ListErrorsComponent,
      ShowAuthedDirective,
      FollowButtonComponent,
      FavoriteButtonComponent,
      ArticleMetaComponent,
      ArticleListComponent,
      ArticlePreviewComponent
  ],
  exports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
      RouterModule,
      ListErrorsComponent,
      ShowAuthedDirective,
      FollowButtonComponent,
      FavoriteButtonComponent,
      ArticleMetaComponent,
      ArticleListComponent,
      ArticlePreviewComponent
  ]
})
export class SharedModule {}
