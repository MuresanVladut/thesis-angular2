import {Injectable} from "@angular/core";
import {ApiService} from "./api.service";
import {Observable} from "rxjs";
import {Article, ArticleListConfig} from "../models/";
import {URLSearchParams} from "@angular/http";


@Injectable()
export class ArticleService{

    constructor(private apiService: ApiService){}

    get(slug): Observable<Article>{
        return this.apiService.get('/articles/' + slug)
            .map(data => data.article);
    }

    recommendations(): Observable<Article[]>{
        return this.apiService.get('/articles/recommendations')
            .map(data => data.articles);
    }

    query(config: ArticleListConfig): Observable<{articles: Article[], articlesCount: number}>{
        // Convert any filters over to Angular's URLSearchParams
        let params: URLSearchParams = new URLSearchParams();

        Object.keys(config.filters).forEach((key) => {
           params.set(key, config.filters[key]);
        });

        return this.apiService
            .get('/articles/' + ((config.type === 'feed') ? '/feed': ''), params)
            .map(data => data);
    }

    save(article): Observable<Article>{
        //If we're updating an existing article
        if(article.slug){
            return this.apiService.put('/articles/' + article.slug, {article: article})
                .map(data => data.article);
        } else {
            return this.apiService.post('/articles/', {article: article})
                .map(data => data.article);
        }
    }

    destroy(slug: string) {
        return this.apiService.delete('/articles/' + slug);
    }

    favorite(slug: string): Observable<Article>{
        return this.apiService.post('/articles/' + slug + '/favorite');
    }

    unfavorite(slug: string): Observable<Article>{
        return this.apiService.delete('/articles/' + slug + '/favorite');
    }
}