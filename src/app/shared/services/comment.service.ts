import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ApiService} from "./api.service";
import {Comment} from '../models/';

@Injectable()
export class CommentService{

    constructor(private apiService: ApiService){}

    add(slug: string, payload: string): Observable<Comment>{
        return this.apiService.post(`/articles/${slug}/comments`, {comment: {body: payload}})
            .map(data => data.comment);
    }

    getAll(slug): Observable<Comment[]> {
        return this.apiService.get(`/articles/${slug}/comments`)
            .map(data => data.comments);
    }

    destroy(commentId: number, articleSlug: string){
        return this.apiService.delete(`/articles/${articleSlug}/comments/${commentId}`);
    }

    latest(){
        return this.apiService.get('/latest')
            .map(data => data.comments);
    }
}