import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {TagsService, UserService, ArticleListConfig, CommentService, ArticleService} from "../shared/";

@Component({
    selector: 'home-page',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
    constructor(
        private router: Router,
        private tagsService: TagsService,
        private userService: UserService,
        private commentService: CommentService,
        private articleService: ArticleService,
    ){}

    isAuthenticated: boolean;
    tags: Array<string> = [];
    tagsLoaded: boolean = false;
    comments: Array<any> = [];
    commentsLoaded: boolean = false;
    recommendations: Array<any> = [];
    recommendationsLoaded: boolean = false;

    listConfig: ArticleListConfig = new ArticleListConfig();

    ngOnInit(): void {
        this.userService.isAuthenticated
            .subscribe(
                (authenticated) =>{
                    this.isAuthenticated = authenticated;

                    // set the article list accordingly
                    if(authenticated){
                        this.setListTo('feed');
                    } else {
                        this.setListTo('all');
                    }
                }
            );
        this.tagsService.getAll()
            .subscribe(
                tags => {
                    this.tags = tags;
                    this.tagsLoaded = true;
                }
            );
        this.commentService.latest()
            .subscribe(
                comments => {
                    this.comments = comments;
                    this.commentsLoaded = true;
                }
            );

        this.articleService.recommendations()
            .subscribe(
              articles => {
                  this.recommendations = articles;
                  this.recommendationsLoaded = true;
              }
            );
    }

    setListTo(type: string = '', filters: Object ={}){
        //If feed is requested but user is not authenticated, redirect to login
        if(type === 'feed' && !this.isAuthenticated){
            this.router.navigateByUrl('/login');
            return;
        }
        // Otherwise set the list object
        this.listConfig = {type: type, filters: filters};
    }

}