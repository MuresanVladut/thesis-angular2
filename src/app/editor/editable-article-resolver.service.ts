import {Injectable} from "@angular/core";
import {Article} from "../shared";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs";
import {UserService, ArticleService} from "../shared";

@Injectable()
export class EditableArticleResolver implements Resolve<Article>{

    constructor(
        private articleService: ArticleService,
        private router: Router,
        private userService: UserService
    ){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{
        return this.articleService.get(route.params['slug'])
            .map(article => {
                    if (this.userService.getCurrentUser().username === article.author.username){
                        return article;
                    } else {
                        this.router.navigateByUrl('/');
                    }
                }
            ).catch((err) => this.router.navigateByUrl('/'));
    }

}