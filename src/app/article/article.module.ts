import {ModuleWithProviders, NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ArticleResolver} from "./article-resolver.service";
import {ArticleComponent} from "./article.component";
import {SharedModule} from "../shared/shared.module";
import {MarkdownPipe} from "./markdown.pipe";
import {ArticleCommentComponent} from "./article-comment.component";

const articleRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'article/:slug',
        component: ArticleComponent,
        resolve: {
            article: ArticleResolver
        }
    }
]);

@NgModule({
    imports: [
        articleRouting,
        SharedModule
    ],
    declarations: [
        ArticleComponent,
        MarkdownPipe,
        ArticleCommentComponent
    ],
    providers: [
        ArticleResolver
    ]
})
export class ArticleModule {}