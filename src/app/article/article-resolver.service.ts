import {Injectable} from "@angular/core";
import {Article, ArticleService, UserService} from "../shared/";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable()
export class ArticleResolver implements Resolve<Article>{

    constructor(
      private articleService: ArticleService,
      private router: Router,
      private userService: UserService,
    ){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{
        return this.articleService.get(route.params['slug'])
            .catch((err) => this.router.navigateByUrl('/'));
    }

}