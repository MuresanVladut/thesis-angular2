import {Component, OnInit} from "@angular/core";
import {
    Article,
    User,
    ArticleService,
    UserService,
    CommentService,
    Comment
} from "../shared";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl} from "@angular/forms";

@Component({
    selector: 'article-page',
    templateUrl: './article.component.html'
})
export class ArticleComponent implements OnInit{

    article: Article;
    currentUser: User;
    canModify: boolean;
    isSubmitting = false;
    isDeleting = false;
    comments: Comment[];
    commentControl = new FormControl();
    commentFormErrors = {};

    constructor(
        private route: ActivatedRoute,
        private articleService: ArticleService,
        private router: Router,
        private userService: UserService,
        private commentService: CommentService
    ){}

    ngOnInit(): void {
        //Loads the prefetched article
        this.route.data.subscribe(
            (data: {article: Article}) => {
                this.article = data.article;
                //Loads the comments on this article
                this.populateComments();
            }
        );
        //Loads the current logged user
        this.userService.currentUser.subscribe(
            (data: User) => {
                this.currentUser = data;
                this.canModify = (this.currentUser.username === this.article.author.username);
            });
    }

    onToggleFavorite(favorited: boolean){
        this.article.favorited = favorited;
        favorited? this.article.favoritesCount++ : this.article.favoritesCount--;
    }

    onToggleFollowing(following: boolean){
        this.article.author.following = following;
    }

    deleteArticle(){
        this.isDeleting = true;

        this.articleService.destroy(this.article.slug)
            .subscribe(
                success =>{
                    this.router.navigateByUrl('/');
                }
            )
    }

    populateComments(){
        this.commentService.getAll(this.article.slug)
            .subscribe(comments => this.comments = comments);
    }

    addComment(){
        this.isSubmitting = true;
        this.commentFormErrors = {};

        const commentBody = this.commentControl.value;
        this.commentService
            .add(this.article.slug, commentBody)
            .subscribe(
                comment => {
                    this.comments.unshift(comment);
                    this.commentControl.reset('');
                    this.isSubmitting = false;
                },
                errors => {
                    this.isSubmitting = true;
                    this.commentFormErrors = errors;
                }
            )
    }

    onDeleteComment(comment: Comment){
        this.commentService.destroy(comment.id, this.article.slug).
            subscribe(
                success => {
                    this.comments = this.comments.filter((item) => item !== comment);
                }
        )
    }
}