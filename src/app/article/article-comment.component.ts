import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {UserService, User, Comment} from "../shared";

@Component({
    selector: 'article-comment',
    templateUrl: './article-comment.component.html'
})
export class ArticleCommentComponent implements OnInit{

    constructor(
        private userService: UserService
    ){}

    @Input() comment: Comment;
    @Output() deleteComment = new EventEmitter<boolean>();

    canModify: boolean;

    ngOnInit(): void {
        //Loads the current user data
        this.userService.currentUser.subscribe(
            (data: User) => {
                this.canModify = (data.username === this.comment.author.username);
            }
        )
    }

    deleteClicked(){
        this.deleteComment.emit(true);
    }

}