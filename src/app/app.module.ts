import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ArticleModule } from './article/article.module';
import { AuthModule } from './auth/auth.module';
import { EditorModule } from './editor/editor.module';
import { HomeModule } from './home/home.module';
import { ProfileModule } from './profile/profile.module';
import { SettingsModule } from './settings/settings.module';
import {
    ApiService,
    ArticleService,
    AuthGuard,
    CommentService,
    FooterComponent,
    HeaderComponent,
    JwtService,
    ProfileService,
    SharedModule,
    TagsService,
    UserService
} from './shared';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([], { useHash: true });

@NgModule({
    declarations: [
        AppComponent,
        FooterComponent,
        HeaderComponent
    ],
    imports: [
        BrowserModule,
        ArticleModule,
        AuthModule,
        EditorModule,
        HomeModule,
        ProfileModule,
        rootRouting,
        SharedModule,
        SettingsModule
    ],
    providers: [
        ApiService,
        ArticleService,
        AuthGuard,
        CommentService,
        JwtService,
        ProfileService,
        TagsService,
        UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}